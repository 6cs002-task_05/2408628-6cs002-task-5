package game;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import game.gameData.Game;

public class gameFilter {

    public static List<gameData.Game> filterByModelName(List<gameData.Game> games, String modelName) {
    	
    	if (games == null) {
            return Collections.emptyList();
        } else {
            return games.stream()
                    .filter(game -> game.getTitle().contains(modelName) ||
                    		game.getGenre().contains(modelName) ||
                            String.valueOf(game.getReleaseYear()).contains(modelName) ||
                            game.getDirector().contains(modelName))
                    .collect(Collectors.toList());
        }
    }

    public static List<gameData.Game> filterWithParallelStream(List<gameData.Game> games, String modelName) {
        return games.parallelStream()
                .filter(game -> game.getTitle().startsWith(modelName) ||
                		game.getGenre().endsWith(modelName) ||
                        String.valueOf(game.getReleaseYear()).endsWith(modelName) ||
                        game.getDirector().contains(modelName) ||
                        game.getTitle().matches(modelName))
                .collect(Collectors.toList());
    }

    public static List<gameData.Game> filterWithSequentialStream(List<gameData.Game> games, String modelName) {
        return games.stream()
                .filter(game -> game.getTitle().startsWith(modelName) ||
                		game.getGenre().endsWith(modelName) ||
                        String.valueOf(game.getReleaseYear()).endsWith(modelName) ||
                        game.getDirector().contains(modelName) ||
                        game.getTitle().matches(modelName))
                .collect(Collectors.toList());
    }

	public static void printGameDetails(List<Game> gameList) {		
	}
}
