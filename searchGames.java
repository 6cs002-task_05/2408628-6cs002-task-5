package game;

import java.util.List;
import java.util.Scanner;

public class searchGames {

    private static String choice;

    public static void main(String[] args) {
        System.out.println("Enter your choice");
        Scanner scanner = new Scanner(System.in);
        choice = scanner.nextLine();
        if (choice != null) {
            dataSet dataSet = new dataSet(); 
            List<gameData.Game> gameList = gameFilter.filterByModelName(dataSet.dataSet, choice);
            List<gameData.Game> parallelGameList = gameFilter.filterWithParallelStream(dataSet.dataSet, choice);
            List<gameData.Game> sequentialGameList = gameFilter.filterWithSequentialStream(dataSet.dataSet, choice);

            System.out.println("Your choice is:");
            printGameDetails(gameList);

            System.out.println("Parallel Stream choice is:");
            printGameDetails(parallelGameList);

            System.out.println("Sequential Stream choice is:");
            printGameDetails(sequentialGameList);
        }
    }

    private static void printGameDetails(List<gameData.Game> games) {
        for (gameData.Game game : games) {
            System.out.println(game);
        }
        System.out.println();
    }
}

