package game;

import java.util.Arrays;
import java.util.List;

import game.gameData.Game;

public class dataSet {

    public static List<gameData.Game> createGameList() {
        return Arrays.asList(
        		new gameData.Game("Interstellar", "Sci-Fi", 2014, "Christopher Nolan", 169),
                new gameData.Game("The Godfather", "Crime", 1972, "Francis Ford Coppola", 175),
                new gameData.Game("The Matrix", "Action", 1999, "The Wachowskis", 136),
                new gameData.Game("Schindler's List", "Biography", 1993, "Steven Spielberg", 195),
                new gameData.Game("The Shawshank Redemption", "Drama", 1994, "Frank Darabont", 142)
        );
    }

	public List<Game> gameData;
	public List<Game> dataSet;
	
	public dataSet() {
        dataSet = createGameList();
    }
}

