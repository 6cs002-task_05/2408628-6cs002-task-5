public class doc1 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("data/lion-king.txt"));

    r.lines().forEach(l -> System.out.println(l));

    r.close();
  }

}
