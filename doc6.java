public class doc6 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
       new BufferedReader(new FileReader("data/lion-king.txt"));

    System.out.println(r.lines().reduce("", String::concat));
    
    r.close();
  }

}
