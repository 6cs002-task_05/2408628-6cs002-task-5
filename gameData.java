package game;

import java.util.Arrays;
import java.util.List;

public class gameData {

    public static class Game {
        private final String title;
        private final String genre;
        private final int releaseYear;
        private final String director;
        private final int playtime; // in hours

        public Game(String title, String genre, int releaseYear, String director, int playtime) {
            this.title = title;
            this.genre = genre;
            this.releaseYear = releaseYear;
            this.director = director;
            this.playtime = playtime;
        }

        public String getTitle() {
            return title;
        }

        public String getGenre() {
            return genre;
        }

        public int getReleaseYear() {
            return releaseYear;
        }

        public String getDirector() {
            return director;
        }

        public int getPlaytime() {
            return playtime;
        }

        @Override
        public String toString() {
            return String.format("\n\"Title: %s\n\"Genre: %s\n\"Release Year: %d\n\"Director: %s\n\"Playtime: %d hours\n",
                    title, genre, releaseYear, director, playtime);
        }
    }

    public static class GameCollection {
        List<Game> games;

        public GameCollection(Game... games) {
            this.games = Arrays.asList(games);
        }
    }
}

